rt-tests (2.4-1) unstable; urgency=medium

  * Update to new upstream release v2.4

 -- Punit Agrawal <punit@debian.org>  Sat, 23 Jul 2022 06:37:41 +0100

rt-tests (2.3-1) unstable; urgency=medium

  * Update copyright to account for file rename
  * Update to new upstream release v2.3

 -- Punit Agrawal <punit@debian.org>  Sat, 21 May 2022 17:06:28 +0100

rt-tests (2.2-1) unstable; urgency=medium

  [ Anders Roxell ]
  * update to new upstream release v1.9
  * update patches for new upstream release
  * Makefile: manpages: only add get_cyclictest_snapshot if PYLIB

  [ Punit Agrawal ]
  * Add Punit Agrawal as an uploader for rt-tests package
  * Update package repository location
  * Update to new upstream release v2.2 (Closes: #971590)
  * Refresh patches for new upstream release

 -- Punit Agrawal <punit@debian.org>  Thu, 09 Sep 2021 13:42:11 +0900

rt-tests (1.5-2) unstable; urgency=medium

  * drop Conflicts to xenomai-runtime which only exists in Jessie.
  * Fix FTBFS on all platforms but arm{el,hf} and x86.

 -- Uwe Kleine-König <ukleinek@debian.org>  Sun, 08 Dec 2019 22:15:22 +0100

rt-tests (1.5-1) unstable; urgency=medium

  * Fix selection for building with or without NUMA (Closes: #930836)
  * New upstream release making several patches unnecessary:
    - dont-build-backfile: done independently in commit 2829763fdd79
      ("rt-tests: Remove install and build of backfire and sendme")
    - install_manpage_rt-migrate-test: Corresponds to upstream commit
      dff174f994f5 ("rt-tests: Makefile: Add missing install of
      rt-migrate-test.8 man page")
    - manpage_pip_stress: Upstream provides a manpage since commit
      c2cf910af821 ("rt-tests: pip_stress: Add an initial man page for
      pip_stress")
    Refresh remaining patches and d/copyright.
    Also upstream switched to python3 (Closes: #945734).
  * Adapt VCS-Browser and VCS-Git to reality.
  * Move to Priority: optional (as per Policy section 2.5)
  * Bump Standards-Version to 4.4.1.
  * Depend on python3 instead of python as all scripts use the new interpreter
    version now.
  * Update d/watch to use https.
  * Enable hardening
  * bump debhelper compat level to 12

 -- Uwe Kleine-König <ukleinek@debian.org>  Thu, 05 Dec 2019 17:08:10 +0100

rt-tests (1.0-3) unstable; urgency=medium

  * drop Recommends to dropped package backfire-dkms (Closes: #869960).

 -- Uwe Kleine-König <ukleinek@debian.org>  Fri, 28 Jul 2017 08:10:02 +0200

rt-tests (1.0-2) unstable; urgency=medium

  * drop backfire kernel module package (Closes: #867706)

 -- Uwe Kleine-König <ukleinek@debian.org>  Sun, 16 Jul 2017 22:11:31 +0200

rt-tests (1.0-1) unstable; urgency=medium

  * new upstream stable release
  * new patch cherry picked from upstream/master to install rt-migrate-test's
    manpage
  * Standards-Version: 3.9.8 (no changes needed)

 -- Uwe Kleine-König <ukleinek@debian.org>  Sun, 19 Jun 2016 23:46:40 +0200

rt-tests (0.97-1) unstable; urgency=medium

  * debian/watch: check upstream's signature
  * new upstream release 

 -- Uwe Kleine-König <ukleinek@debian.org>  Thu, 14 Apr 2016 09:21:14 +0200

rt-tests (0.96-1) unstable; urgency=medium

  * new upstream release 
    - drop Fix-VERSION-in-rt-migrate-test.patch which was applied for 0.94
  * switch maintainer entry to debian.org address

 -- Uwe Kleine-König <ukleinek@debian.org>  Fri, 22 Jan 2016 22:20:51 +0100

rt-tests (0.93-1) unstable; urgency=medium

  * new upstream release
  * fix version reported by rt-migrate-test -h (patch by John Kacur) 

 -- Uwe Kleine-König <uwe@kleine-koenig.org>  Mon, 17 Aug 2015 20:41:14 +0200

rt-tests (0.92-1) unstable; urgency=medium

  * new upstream release (Closes: #716237)
  * Standards-Version: 3.9.6 (no changes needed)

 -- Uwe Kleine-König <uwe@kleine-koenig.org>  Fri, 12 Jun 2015 22:35:55 +0200

rt-tests (0.89-1) unstable; urgency=medium

  * new upstream release
  * enable numa for mips{64,n32}{,el} (Closes: #756597)

 -- Uwe Kleine-König <uwe@kleine-koenig.org>  Thu, 31 Jul 2014 23:31:39 +0200

rt-tests (0.88-1) unstable; urgency=low

  * new upstream release

 -- Uwe Kleine-König <uwe@kleine-koenig.org>  Fri, 28 Mar 2014 21:48:54 +0100

rt-tests (0.87-1) unstable; urgency=low

  * upstream changed path to released archives
  * new upstream release
  * Standards-Version: 3.9.5 (no changes needed)

 -- Uwe Kleine-König <uwe@kleine-koenig.org>  Sun, 29 Dec 2013 13:22:45 +0100

rt-tests (0.85-1) unstable; urgency=low

  * new upstream release
  * Build-Depends += libnuma-dev for powerpcspe, ppc64 and x32
    (Closes: #711952)
  * Standards-Version: 3.9.4 (no changes needed)

 -- Uwe Kleine-König <uwe@kleine-koenig.org>  Tue, 16 Jul 2013 20:32:07 +0200

rt-tests (0.84-2) unstable; urgency=medium

  * fix backfire kernel module for Linux v3.4-rc1 and later
  * backport  
    	dd6ae11 (hackbench: init child's struct before using it)
    to make hackbench work on armhf. (Closes: #711363)

 -- Uwe Kleine-König <uwe@kleine-koenig.org>  Thu, 06 Jun 2013 21:30:30 +0200

rt-tests (0.84-1) experimental; urgency=low

  * new upstream release
  * convert copyright file to machine-readable copyright format version 1.0
  * Standards-Version: 3.9.3 (no changes needed)

 -- Uwe Kleine-König <u.kleine-koenig@pengutronix.de>  Sun, 02 Sep 2012 23:57:33 +0200

rt-tests (0.83-1) unstable; urgency=low

  * new upstream release

 -- Uwe Kleine-König <u.kleine-koenig@pengutronix.de>  Fri, 02 Dec 2011 10:40:15 +0100

rt-tests (0.74-1) unstable; urgency=low

  * new upstream release
  * Fix dkms warning (Closes: LP: #681908). Patch by eraserix, thanks.
  * add build-{arch,indep} targets to debian/rules
  * new patch providing a man page for pip_stress to make lintian a bit
    happier.
  * provide sequence as first parameter to dh for debhelper compatitility
    level 8 and switch to level 8
  * Standards-Version: 3.9.2 (no changes needed)
  * new patch to fix wording for error message when setting the
    scheduling policy fails (Closes: #619938)
  * drop patch kernvar_fix_possible_buffer_overflow (applied upstream)

 -- Uwe Kleine-König <u.kleine-koenig@pengutronix.de>  Fri, 26 Aug 2011 09:25:37 +0200

rt-tests (0.72-1) unstable; urgency=low

  * new upstream release
  * new patch kernvar_fix_possible_buffer_overflow
  * use Architecture: linux-any (Closes: #604691)
  * Standards-Version: 3.9.1 (no changes needed)

 -- Uwe Kleine-König <u.kleine-koenig@pengutronix.de>  Sun, 20 Mar 2011 16:31:36 +0100

rt-tests (0.71-1) unstable; urgency=low

  * new upstream release
  * drop install_backfire_c_into_srcdir_backfire (applied upstream)
  * drop rename_pip_to_pip_stress (applied upstream)
  * refresh install_hwlatdetect_into_sbindir
  * add powerpcspe to architecture list
  * add backfire package with dkms support

 -- Uwe Kleine-König <u.kleine-koenig@pengutronix.de>  Wed, 02 Jun 2010 22:15:03 +0200

rt-tests (0.66-2) unstable; urgency=high

  * Urgency high due to rc bug fix
  * new patch: rename pip to pip_stress (Closes: #572104)

 -- Uwe Kleine-König <u.kleine-koenig@pengutronix.de>  Mon, 01 Mar 2010 17:02:28 +0100

rt-tests (0.66-1) unstable; urgency=low

  * add sh4 to architecture list (Closes: #555554)
  * new upstream release
  * Standards-Version: 3.8.4 (no changes needed)
  * Switch to dpkg-source 3.0 (quilt) format
  * build with libnuma on platforms that have it
  * new patch: install hwlatdetect directly into $sbindir
  * new patch: install install backfire.c to $(srcdir)/backfire/

 -- Uwe Kleine-König <u.kleine-koenig@pengutronix.de>  Mon, 22 Feb 2010 15:35:03 +0100

rt-tests (0.53-1) unstable; urgency=low

  * new upstream release
  * only build on Linux archs but not hppa (see #534352)

 -- Uwe Kleine-König <u.kleine-koenig@pengutronix.de>  Thu, 29 Oct 2009 21:23:10 +0100

rt-tests (0.51-1) unstable; urgency=low

  * reformat copyright and add authors and years of the copyright
  * new upstream release
  * Standards-Version: 3.8.3 (no changes needed)

 -- Uwe Kleine-König <u.kleine-koenig@pengutronix.de>  Sat, 19 Sep 2009 16:29:10 +0200

rt-tests (0.50-1) unstable; urgency=low

  * new upstream release

 -- Uwe Kleine-König <u.kleine-koenig@pengutronix.de>  Mon, 20 Jul 2009 21:50:08 +0200

rt-tests (0.45-1) unstable; urgency=low

  * new upstream release
  * add dependency on python for hwlatdetect
  * Standards-Version: 3.8.2 (no changes needed)

 -- Uwe Kleine-König <u.kleine-koenig@pengutronix.de>  Tue, 30 Jun 2009 10:19:04 +0200

rt-tests (0.39-2) unstable; urgency=low

  * add VCS URL to control
  * conflict with xenomai-runtime (Closes: #529783)

 -- Uwe Kleine-König <u.kleine-koenig@pengutronix.de>  Mon, 11 May 2009 00:18:37 +0200

rt-tests (0.39-1) unstable; urgency=low

  * Initial release. (Closes: #521895)

 -- Uwe Kleine-König <u.kleine-koenig@pengutronix.de>  Wed, 06 May 2009 21:22:28 +0200
